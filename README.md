# Security

Security scans as pipeline jobs.

## Static Application Security Testing (SAST)

Check your source code for known vulnerabilities.

### Inputs

All inputs are optional for this component.

| Input | Default | Description |
| ----- | ------- | ----------- |
| `stage` | `test` | Define the stage where the `sast` job will be added. |
| `excluded_analyzers` | `""` | List of analyzers to exclude from running. |
| `excluded_paths` | `"spec, test, tests, tmp"` | List of paths to exclude from the scan. |
| `kubernetes_manifest` | `"false"` | Set to `true` to run `kubesec-sast` scan. |

### Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/security/sast@2.1
    with:
      stage: test
      excluded_analyzers: spotbugs
```

Configure SAST with CI/CD variables:
https://docs.gitlab.com/ee/user/application_security/sast/index.html#available-variables

Read more about SAST on GitLab: https://docs.gitlab.com/ee/user/application_security/sast

How to set variables: https://docs.gitlab.com/ee/ci/yaml/#variables

## Secret Detection 

### Inputs

| Input | Default | Description |
| ----- | ------- | ----------- |
| `stage` | `test` | Define the stage where the `secret_detection` job will be added. |

### Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/security/secret-detection@2.1
    with:
      stage: test
```

Configure the scanning tool through the environment variables:
https://docs.gitlab.com/ee/user/application_security/secret_detection/#available-variables

Read more about Secret Detection on GitLab: https://docs.gitlab.com/ee/user/application_security/secret_detection

How to set variables: https://docs.gitlab.com/ee/ci/yaml/#variables